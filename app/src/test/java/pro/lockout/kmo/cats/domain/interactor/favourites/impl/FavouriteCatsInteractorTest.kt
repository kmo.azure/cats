package pro.lockout.kmo.cats.domain.interactor.favourites.impl

import com.nhaarman.mockitokotlin2.*
import io.reactivex.Completable
import io.reactivex.Single
import org.junit.After

import org.junit.Assert.*
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.junit.MockitoJUnitRunner
import pro.lockout.kmo.cats.data.model.cache.CatCacheDataModel
import pro.lockout.kmo.cats.data.repository.favourites.FavouriteCatsRepository
import pro.lockout.kmo.cats.domain.interactor.favourites.FavouriteCatsInteractor
import pro.lockout.kmo.cats.domain.model.CatDomainModel

@RunWith(MockitoJUnitRunner::class)
class FavouriteCatsInteractorTest {

    private val favouriteCatsRepository: FavouriteCatsRepository = mock()

    private val interactor = FavouriteCatsInteractorImpl(favouriteCatsRepository)

    private val favouriteCats: List<CatCacheDataModel> = listOf(
        CatCacheDataModel().apply {
            id = "id1"
            url = "url1"
        },
        CatCacheDataModel().apply {
            id = "id2"
            url = "url2"
        },
        CatCacheDataModel().apply {
            id = "id3"
            url = "url3"
        })

    @Test
    fun `WHEN fetch EXPECT favourite cats`() {
        whenever(favouriteCatsRepository.fetch()).thenReturn(Single.just(favouriteCats))

        interactor.fetch()
            .test()
            .assertValue {
                it[0].id == favouriteCats[0].id && it[1].id == favouriteCats[1].id && it[2].id == favouriteCats[2].id
            }

        verify(favouriteCatsRepository).fetch()
    }

    @Test
    fun `WHEN retain EXPECT add cat to favourites`() {
        val cat = CatDomainModel("id1", "url1", true)

        whenever(favouriteCatsRepository.retain(any())).thenReturn(Completable.complete())

        interactor.retain(cat)
            .test()
            .assertComplete()

        verify(favouriteCatsRepository).retain(any())
    }

    @After
    fun tearDown() {
        verifyNoMoreInteractions(favouriteCatsRepository)
    }

}