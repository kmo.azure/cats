package pro.lockout.kmo.cats.data.repository.download.impl

import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.verify
import com.nhaarman.mockitokotlin2.verifyNoMoreInteractions
import com.nhaarman.mockitokotlin2.whenever
import io.reactivex.Completable
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.junit.MockitoJUnitRunner
import pro.lockout.kmo.cats.data.repository.download.datasource.DownloadDataSource

@RunWith(MockitoJUnitRunner::class)
class DownloadRepositoryTest {

    private val dataSource: DownloadDataSource = mock()
    private val repository = DownloadRepositoryImpl(dataSource)

    @Test
    fun `WHEN download EXPECT download file`() {
        val url = "url"

        whenever(dataSource.download(url)).thenReturn(Completable.complete())

        repository.download(url)
            .test()
            .assertComplete()

        verify(dataSource).download(url)
        verifyNoMoreInteractions(dataSource)
    }

}