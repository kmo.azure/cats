package pro.lockout.kmo.cats.schedulers

import io.reactivex.Scheduler
import pro.lockout.kmo.cats.auxiliary.schedulers.Schedulers

class TestSchedulers: Schedulers {

    override fun ui(): Scheduler =
        io.reactivex.schedulers.Schedulers.trampoline()

    override fun background(): Scheduler =
        io.reactivex.schedulers.Schedulers.trampoline()

}