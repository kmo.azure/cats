package pro.lockout.kmo.cats.data.repository.favourites.impl

import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.verify
import com.nhaarman.mockitokotlin2.verifyNoMoreInteractions
import com.nhaarman.mockitokotlin2.whenever
import io.reactivex.Completable
import io.reactivex.Single
import org.junit.After
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.junit.MockitoJUnitRunner
import pro.lockout.kmo.cats.data.model.cache.CatCacheDataModel
import pro.lockout.kmo.cats.data.repository.favourites.datasource.FavouriteCatsDataSource

@RunWith(MockitoJUnitRunner::class)
class FavouriteCatsRepositoryTest {

    private val dataSource: FavouriteCatsDataSource = mock()
    private val repository = FavouriteCatsRepositoryImpl(dataSource)

    private val favouriteCats: List<CatCacheDataModel> = listOf(
        CatCacheDataModel().apply { id = "id1" },
        CatCacheDataModel().apply { id = "id2" },
        CatCacheDataModel().apply { id = "id3" })

    @Test
    fun `WHEN fetch data EXPECT fetch favourite cats`() {
        whenever(dataSource.fetch()).thenReturn(Single.just(favouriteCats))

        repository.fetch()
            .test()
            .assertValue(favouriteCats)

        verify(dataSource).fetch()
    }

    @Test
    fun `WHEN retain data EXPECT add cat to favourites`() {
        whenever(dataSource.retain(favouriteCats[0])).thenReturn(Completable.complete())

        repository.retain(favouriteCats[0])
            .test()
            .assertComplete()

        verify(dataSource).retain(favouriteCats[0])
    }

    @After
    fun tearDown() {
        verifyNoMoreInteractions(dataSource)
    }

}