package pro.lockout.kmo.cats.presentation.cats.impl

import com.nhaarman.mockitokotlin2.*
import io.reactivex.Completable
import io.reactivex.Single
import io.reactivex.schedulers.TestScheduler
import org.junit.After
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.junit.MockitoJUnitRunner
import pro.lockout.kmo.cats.auxiliary.schedulers.Schedulers
import pro.lockout.kmo.cats.domain.interactor.cats.CatsInteractor
import pro.lockout.kmo.cats.domain.interactor.download.DownloadInteractor
import pro.lockout.kmo.cats.domain.interactor.favourites.FavouriteCatsInteractor
import pro.lockout.kmo.cats.domain.model.CatDomainModel
import pro.lockout.kmo.cats.presentation.cats.CatsRouter
import pro.lockout.kmo.cats.presentation.cats.CatsView
import pro.lockout.kmo.cats.presentation.model.CatViewModel
import pro.lockout.kmo.cats.schedulers.TestSchedulers
import java.lang.Exception

@RunWith(MockitoJUnitRunner::class)
class CatsPresenterTest {

    private val catsInteractor: CatsInteractor = mock()
    private val favouriteCatsInteractor: FavouriteCatsInteractor = mock()
    private val downloadInteractor: DownloadInteractor = mock()
    private val router: CatsRouter = mock()
    private val view: CatsView = mock()
    private val schedulers: Schedulers = TestSchedulers()

    private val presenter = CatsPresenterImpl(
        catsInteractor,
        favouriteCatsInteractor,
        downloadInteractor,
        schedulers,
        router)

    private val cats: List<CatDomainModel> = listOf(
        CatDomainModel("id1", "url1", false),
        CatDomainModel("id2", "url2", true),
        CatDomainModel("id3", "url3", null))

    @Test
    fun `WHEN attach view EXPECT success load cats`() {
        val currentCatPosition = 0
        val catsCount = 0

        whenever(catsInteractor.fetch(currentCatPosition, catsCount))
            .thenReturn(Single.just(cats))

        presenter.attachView(view)

        verify(view).showLoading()
        verify(catsInteractor).fetch(currentCatPosition, catsCount)
        verify(view).showCats(argWhere { it[0].id == cats[0].id && it[1].id == cats[1].id && it[2].id == cats[2].id })
    }

    @Test
    fun `WHEN attach view EXPECT error load cats`() {
        val exception = Exception()
        val currentCatPosition = 0
        val catsCount = 0

        whenever(catsInteractor.fetch(currentCatPosition, catsCount))
            .thenReturn(Single.error(exception))

        presenter.attachView(view)

        verify(view).showLoading()
        verify(catsInteractor).fetch(currentCatPosition, catsCount)
        verify(view).showError(eq(exception.message))
    }

    @Test
    fun `WHEN add cat to favourites EXPECT successful`() {
        val favouriteCat = CatViewModel("id1", "url1", false)

        whenever(favouriteCatsInteractor.retain(cats[0])).thenReturn(Completable.complete())

        presenter.addFavouriteCat(favouriteCat)

        verify(favouriteCatsInteractor).retain(cats[0])
    }

    @Test
    fun `WHEN download document EXPECT successful`() {
        val url = "download url"

        whenever(downloadInteractor.download(url)).thenReturn(Completable.complete())

        presenter.download(url)

        verify(downloadInteractor).download(url)
    }

    @Test
    fun `WHEN go to favourite cats EXPECT open screen`() {
        presenter.toFavourites()

        verify(router).toFavouriteCats()
    }

    @After
    fun tearDown() {
        verifyNoMoreInteractions(
            view,
            catsInteractor,
            favouriteCatsInteractor,
            downloadInteractor,
            router)
    }

}