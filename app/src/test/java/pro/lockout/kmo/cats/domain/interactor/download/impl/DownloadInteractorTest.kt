package pro.lockout.kmo.cats.domain.interactor.download.impl

import com.nhaarman.mockitokotlin2.*
import io.reactivex.Completable
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.junit.MockitoJUnitRunner
import pro.lockout.kmo.cats.data.repository.download.DownloadRepository

@RunWith(MockitoJUnitRunner::class)
class DownloadInteractorTest {

    private val downloadRepository: DownloadRepository = mock()
    private val interactor = DownloadInteractorImpl(downloadRepository)

    @Test
    fun `WHEN download EXPECT successful`() {
        val url = "url"

        whenever(downloadRepository.download(url)).thenReturn(Completable.complete())

        interactor.download(url)
            .test()
            .assertComplete()

        verify(downloadRepository).download(eq(url))
        verifyNoMoreInteractions(downloadRepository)
    }

}