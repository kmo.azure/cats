package pro.lockout.kmo.cats.presentation.favourites.impl

import com.nhaarman.mockitokotlin2.*
import io.reactivex.Completable
import io.reactivex.Single
import org.junit.After
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.junit.MockitoJUnitRunner
import pro.lockout.kmo.cats.auxiliary.schedulers.Schedulers
import pro.lockout.kmo.cats.domain.interactor.download.DownloadInteractor
import pro.lockout.kmo.cats.domain.interactor.favourites.FavouriteCatsInteractor
import pro.lockout.kmo.cats.domain.model.CatDomainModel
import pro.lockout.kmo.cats.presentation.favourites.FavouriteCatsView
import pro.lockout.kmo.cats.schedulers.TestSchedulers
import java.lang.Exception

@RunWith(MockitoJUnitRunner::class)
class FavouriteCatsPresenterTest {

    private val favouriteCatsInteractor: FavouriteCatsInteractor = mock()
    private val downloadInteractor: DownloadInteractor = mock()
    private val schedulers: Schedulers = TestSchedulers()
    private val view: FavouriteCatsView = mock()

    private val presenter = FavouriteCatsPresenterImpl(
        favouriteCatsInteractor,
        downloadInteractor,
        schedulers)

    private val favouriteCats: List<CatDomainModel> = listOf(
        CatDomainModel("id1", "url1", false),
        CatDomainModel("id2", "url2", true),
        CatDomainModel("id3", "url3", null))

    @Test
    fun `WHEN attach view EXPECT load favourite cats successful`() {
        whenever(favouriteCatsInteractor.fetch()).thenReturn(Single.just(favouriteCats))

        presenter.attachView(view)

        verify(view).showLoading()
        verify(favouriteCatsInteractor).fetch()
        verify(view).showFavouriteCats(argWhere {
            it[0].id == favouriteCats[0].id && it[1].id == favouriteCats[1].id && it[2].id == favouriteCats[2].id })
    }

    @Test
    fun `WHEN attach view EXPECT load favourite cats with error`() {
        val error = Exception()

        whenever(favouriteCatsInteractor.fetch()).thenReturn(Single.error(error))

        presenter.attachView(view)

        verify(view).showLoading()
        verify(favouriteCatsInteractor).fetch()
        verify(view).showError(eq(error.message))
    }

    @Test
    fun `WHEN download document EXPECT successful`() {
        val url = "download url"

        whenever(downloadInteractor.download(url)).thenReturn(Completable.complete())

        presenter.download(url)

        verify(downloadInteractor).download(url)
    }

    @After
    fun tearDown() {
        verifyNoMoreInteractions(
            view,
            favouriteCatsInteractor,
            downloadInteractor)
    }

}