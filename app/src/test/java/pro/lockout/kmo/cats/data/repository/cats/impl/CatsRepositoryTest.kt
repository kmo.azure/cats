package pro.lockout.kmo.cats.data.repository.cats.impl

import com.nhaarman.mockitokotlin2.*
import io.reactivex.Single
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.junit.MockitoJUnitRunner
import pro.lockout.kmo.cats.data.model.cloud.CatCloudDataModel
import pro.lockout.kmo.cats.data.repository.cats.datasource.CatsDataSource

@RunWith(MockitoJUnitRunner::class)
class CatsRepositoryTest {

    private val dataSource: CatsDataSource = mock()
    private val repository = CatsRepositoryImpl(dataSource)

    private val cats: List<CatCloudDataModel> = listOf(
        CatCloudDataModel("id1", "url1"),
        CatCloudDataModel("id2", "url2"),
        CatCloudDataModel("id3", "url3"))

    @Test
    fun `WHEN fetch data EXPECT load cats`() {
        val limit = 10
        val page = 10

        whenever(dataSource.fetch(limit, page)).thenReturn(Single.just(cats))

        repository.fetch(limit, page)
            .test()
            .assertValue(cats)

        verify(dataSource).fetch(limit, page)
        verifyNoMoreInteractions(dataSource)
    }

}