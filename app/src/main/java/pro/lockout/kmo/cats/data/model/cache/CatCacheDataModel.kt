package pro.lockout.kmo.cats.data.model.cache

import io.realm.RealmObject
import pro.lockout.kmo.cats.domain.model.CatDomainModel

open class CatCacheDataModel: RealmObject() {

    companion object {

        fun map(cat: CatDomainModel): CatCacheDataModel =
            CatCacheDataModel().apply {
                id = cat.id
                url = cat.url
            }

        fun map(cats: List<CatDomainModel>): List<CatCacheDataModel> =
            cats.map(::map)

    }

    var id: String = ""
    var url: String = ""

}