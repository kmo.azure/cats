package pro.lockout.kmo.cats.data.exception

import java.lang.Exception

class FavouriteCatsNotFoundException(
    private val exceptionMessage: String):
        Exception() {

    override val message: String?
        get() = exceptionMessage

}