package pro.lockout.kmo.cats.presentation.cats.impl

import io.reactivex.disposables.CompositeDisposable
import pro.lockout.kmo.cats.auxiliary.extensions.addTo
import pro.lockout.kmo.cats.auxiliary.schedulers.Schedulers
import pro.lockout.kmo.cats.domain.interactor.cats.CatsInteractor
import pro.lockout.kmo.cats.domain.interactor.download.DownloadInteractor
import pro.lockout.kmo.cats.domain.interactor.favourites.FavouriteCatsInteractor
import pro.lockout.kmo.cats.presentation.cats.CatsPresenter
import pro.lockout.kmo.cats.presentation.cats.CatsRouter
import pro.lockout.kmo.cats.presentation.cats.CatsView
import pro.lockout.kmo.cats.presentation.model.CatViewModel

class CatsPresenterImpl(
    private val catsInteractor: CatsInteractor,
    private val favouriteCatsInteractor: FavouriteCatsInteractor,
    private val downloadInteractor: DownloadInteractor,
    private val schedulers: Schedulers,
    private val router: CatsRouter):
        CatsPresenter {

    override var attachedView: CatsView? = null
    override var disposable = CompositeDisposable()

    override fun attachView(view: CatsView?) {
        super.attachView(view)

        attachedView?.showLoading()
        loadCats()
    }

    override fun displayCats(catPosition: Int, catsCount: Int) {
        loadCats(catPosition, catsCount)
    }

    private fun loadCats(currentCatPosition: Int = 0, catsCount: Int = 0) {
        catsInteractor.fetch(currentCatPosition, catsCount)
            .map(CatViewModel.Companion::map)
            .observeOn(schedulers.ui())
            .subscribeOn(schedulers.background())
            .subscribe(
                { attachedView?.showCats(it) },
                { attachedView?.showError(it.message) })
            .addTo(disposable)
    }

    override fun addFavouriteCat(cat: CatViewModel) {
        favouriteCatsInteractor.retain(CatViewModel.map(cat))
            .observeOn(schedulers.ui())
            .subscribeOn(schedulers.background())
            .subscribe()
            .addTo(disposable)
    }

    override fun download(url: String) {
        downloadInteractor.download(url)
            .observeOn(schedulers.ui())
            .subscribeOn(schedulers.background())
            .subscribe()
            .addTo(disposable)
    }

    override fun toFavourites() {
        router.toFavouriteCats()
    }

}