package pro.lockout.kmo.cats.auxiliary.di.modules

import dagger.Module
import dagger.Provides
import pro.lockout.kmo.cats.data.repository.cats.CatsRepository
import pro.lockout.kmo.cats.data.repository.download.DownloadRepository
import pro.lockout.kmo.cats.data.repository.favourites.FavouriteCatsRepository
import pro.lockout.kmo.cats.domain.interactor.cats.CatsInteractor
import pro.lockout.kmo.cats.domain.interactor.cats.impl.CatsInteractorImpl
import pro.lockout.kmo.cats.domain.interactor.download.DownloadInteractor
import pro.lockout.kmo.cats.domain.interactor.download.impl.DownloadInteractorImpl
import pro.lockout.kmo.cats.domain.interactor.favourites.FavouriteCatsInteractor
import pro.lockout.kmo.cats.domain.interactor.favourites.impl.FavouriteCatsInteractorImpl
import javax.inject.Singleton

@Module(includes = [ DataModule::class ])
class DomainModule {

    @Singleton
    @Provides
    fun provideCatsInteractor(
        catsRepository: CatsRepository): CatsInteractor =
            CatsInteractorImpl(catsRepository)

    @Singleton
    @Provides
    fun provideFavouriteCatsInteractor(
        favouriteCatsRepository: FavouriteCatsRepository): FavouriteCatsInteractor =
            FavouriteCatsInteractorImpl(favouriteCatsRepository)

    @Singleton
    @Provides
    fun provideDownloadInteractor(
        downloadRepository: DownloadRepository): DownloadInteractor =
            DownloadInteractorImpl(downloadRepository)

}