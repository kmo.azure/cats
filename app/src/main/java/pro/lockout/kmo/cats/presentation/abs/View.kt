package pro.lockout.kmo.cats.presentation.abs

interface View {

    /**
     * Показывает экрна загрузки
     */
    fun showLoading() { /* Пустая реализация */ }

    /**
     * Показывает содержимое презентера
     */
    fun showContent() { /* Пустая реализация */ }

    /**
     * Показывает сообщение об ошибке
     *
     * @param errorValue Информация об ошибке
     */
    fun showError(errorValue: Any?) { /* Пустая реализация */ }

}