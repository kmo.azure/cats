package pro.lockout.kmo.cats.domain.interactor.cats

import io.reactivex.Single
import pro.lockout.kmo.cats.domain.model.CatDomainModel

interface CatsInteractor {

    /**
     * Получение списка котов
     *
     * @param currentCatPosition текущий кот
     * в списке
     * @param catsCount количество котов на
     * текущий момент
     *
     * @return список котов
     */
    fun fetch(currentCatPosition: Int, catsCount: Int): Single<List<CatDomainModel>>

}