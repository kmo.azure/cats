package pro.lockout.kmo.cats.presentation

import android.os.Bundle
import dagger.android.AndroidInjection
import dagger.android.support.DaggerAppCompatActivity
import dagger.android.support.HasSupportFragmentInjector
import pro.lockout.kmo.cats.presentation.abs.Router
import pro.lockout.kmo.cats.presentation.cats.impl.CatsFragment

class MainActivity : DaggerAppCompatActivity(), HasSupportFragmentInjector {

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)

        supportActionBar?.hide()
        initStartFragment()
    }

    private fun initStartFragment() {
        supportFragmentManager
            .beginTransaction()
            .add(
                android.R.id.content,
                CatsFragment.newInstance(),
                CatsFragment::class.java.name)
            .commit()
    }

    override fun onBackPressed() {
        if (supportFragmentManager.backStackEntryCount != 0) {
            supportFragmentManager.popBackStack()
        } else {
            super.onBackPressed()
        }
    }

}
