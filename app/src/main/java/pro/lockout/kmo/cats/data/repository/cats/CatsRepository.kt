package pro.lockout.kmo.cats.data.repository.cats

import io.reactivex.Single
import pro.lockout.kmo.cats.data.model.cloud.CatCloudDataModel

interface CatsRepository {

    /**
     * Получение списка котов
     *
     * @param page страница для загрузки котов
     * @param limit количество котов получаемых
     * в одном запросе
     *
     * @return список котов
     */
    fun fetch(limit: Int, page: Int): Single<List<CatCloudDataModel>>

}