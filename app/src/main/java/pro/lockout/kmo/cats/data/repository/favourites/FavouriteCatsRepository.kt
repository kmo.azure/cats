package pro.lockout.kmo.cats.data.repository.favourites

import io.reactivex.Completable
import io.reactivex.Single
import pro.lockout.kmo.cats.data.model.cache.CatCacheDataModel

interface FavouriteCatsRepository {

    /**
     * Получение списка избранных котов
     *
     * @return список избранных котов
     */
    fun fetch(): Single<List<CatCacheDataModel>>

    /**
     * Добавление кота в избранное
     *
     * @param cat выбранный кот
     */
    fun retain(cat: CatCacheDataModel): Completable

}