package pro.lockout.kmo.cats.presentation.auxiliary.widget

import android.content.Context
import android.os.Bundle
import android.os.Parcelable
import android.util.AttributeSet
import android.view.View
import androidx.recyclerview.widget.RecyclerView

class StatefulRecyclerView: RecyclerView {

    companion object {

        private const val SAVED_SUPER_STATE = "super-state"

        private const val SAVED_LAYOUT_MANAGER = "layout-manager-state"

    }

    constructor(context: Context): super(context)

    constructor(context: Context, attrs: AttributeSet): super(context, attrs)

    constructor(context: Context, attrs: AttributeSet, defStyle: Int): super(context, attrs, defStyle)

    init {

        overScrollMode = View.OVER_SCROLL_NEVER

        isMotionEventSplittingEnabled = false

    }

    private var layoutManagerSavedInstance: Parcelable? = null

    override fun onSaveInstanceState(): Parcelable = Bundle().apply {
        putParcelable(SAVED_SUPER_STATE, super.onSaveInstanceState())
        layoutManager?.let {
            putParcelable(SAVED_LAYOUT_MANAGER, it.onSaveInstanceState())
        }
    }

    override fun onRestoreInstanceState(state: Parcelable?) {
        if (state is Bundle) {
            layoutManagerSavedInstance = state.getParcelable(SAVED_LAYOUT_MANAGER)
        }

        super.onRestoreInstanceState((state as Bundle).getParcelable(SAVED_SUPER_STATE))
    }

    private fun restorePosition() {
        if (layoutManagerSavedInstance != null) {
            layoutManager?.onRestoreInstanceState(layoutManagerSavedInstance)
            layoutManagerSavedInstance = null
        }
    }

    private val adapterDataObserver = object: AdapterDataObserver() {

        override fun onChanged() = restorePosition()

        override fun onItemRangeRemoved(positionStart: Int, itemCount: Int) = restorePosition()

        override fun onItemRangeMoved(fromPosition: Int, toPosition: Int, itemCount: Int) = restorePosition()

        override fun onItemRangeInserted(positionStart: Int, itemCount: Int) = restorePosition()

        override fun onItemRangeChanged(positionStart: Int, itemCount: Int) = restorePosition()

        override fun onItemRangeChanged(positionStart: Int, itemCount: Int, payload: Any?) = restorePosition()

    }

    override fun setAdapter(adapter: Adapter<*>?) {
        super.setAdapter(adapter)
        this.adapter?.registerAdapterDataObserver(adapterDataObserver)
    }

}