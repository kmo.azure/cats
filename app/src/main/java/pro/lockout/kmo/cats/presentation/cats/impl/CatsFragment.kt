package pro.lockout.kmo.cats.presentation.cats.impl

import android.Manifest.permission.WRITE_EXTERNAL_STORAGE
import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import kotlinx.android.synthetic.main.cats.*
import kotlinx.android.synthetic.main.error.*
import kotlinx.android.synthetic.main.loading.*
import pro.lockout.kmo.cats.R
import pro.lockout.kmo.cats.auxiliary.extensions.gone
import pro.lockout.kmo.cats.auxiliary.extensions.visible
import pro.lockout.kmo.cats.domain.interactor.cats.CatsInteractor
import pro.lockout.kmo.cats.domain.interactor.download.DownloadInteractor
import pro.lockout.kmo.cats.domain.interactor.favourites.FavouriteCatsInteractor
import pro.lockout.kmo.cats.presentation.abs.AbsFragment
import pro.lockout.kmo.cats.presentation.cats.CatsPresenter
import pro.lockout.kmo.cats.presentation.cats.CatsView
import pro.lockout.kmo.cats.presentation.cats.impl.adapter.CatsAdapter
import pro.lockout.kmo.cats.presentation.model.CatViewModel
import javax.inject.Inject

class CatsFragment: AbsFragment(), CatsView,
    CatsAdapter.Delegate,
    CatsAdapter.DownloadDelegate {

    companion object {

        fun newInstance(): Fragment = CatsFragment().apply {
            arguments = Bundle.EMPTY
        }

    }

    override val layout: Int = R.layout.cats

    @Inject lateinit var catsInteractor: CatsInteractor
    @Inject lateinit var favouriteCatsInteractor: FavouriteCatsInteractor
    @Inject lateinit var downloadInteractor: DownloadInteractor

    private val presenter: CatsPresenter by lazy {
        CatsPresenterImpl(catsInteractor, favouriteCatsInteractor,
            downloadInteractor, schedulers, CatsRouterImpl(fragmentManager))
    }

    private val catsAdapter = CatsAdapter(delegate = this, downloadDelegate = this)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        favourites.setOnClickListener { presenter.toFavourites() }

        catList.adapter = catsAdapter

        presenter.attachView(this)
    }

    override fun showCats(cats: List<CatViewModel>) {
        catsAdapter.updateData(cats)
        showContent()
    }

    override fun onUploadCats(catPosition: Int) {
        presenter.displayCats(catPosition, catsAdapter.itemCount)
    }

    override fun onAddFavouritePicked(cat: CatViewModel) {
        presenter.addFavouriteCat(cat)
    }

    override fun onDownloadImagePicket(url: String) {
        checkPermission(WRITE_EXTERNAL_STORAGE) {
            presenter.download(url)
        }
    }

    override fun showLoading() {
        error.gone()
        loading.visible()
    }

    override fun showContent() {
        error.gone()
        loading.gone()
    }

    override fun showError(errorValue: Any?) {
        (errorValue as? String)?.let {
            error.text = errorValue
        }

        error.visible()
        loading.gone()
    }

    override fun onDestroyView() {
        presenter.destroyView()

        super.onDestroyView()
    }

}