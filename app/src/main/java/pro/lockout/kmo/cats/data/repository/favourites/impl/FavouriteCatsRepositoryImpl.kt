package pro.lockout.kmo.cats.data.repository.favourites.impl

import io.reactivex.Completable
import io.reactivex.Single
import pro.lockout.kmo.cats.data.model.cache.CatCacheDataModel
import pro.lockout.kmo.cats.data.repository.favourites.FavouriteCatsRepository
import pro.lockout.kmo.cats.data.repository.favourites.datasource.FavouriteCatsDataSource
import javax.inject.Inject

class FavouriteCatsRepositoryImpl
    @Inject constructor(
        private val cache: FavouriteCatsDataSource):
            FavouriteCatsRepository {

    override fun fetch(): Single<List<CatCacheDataModel>> =
        cache.fetch()

    override fun retain(cat: CatCacheDataModel): Completable =
        cache.retain(cat)

}