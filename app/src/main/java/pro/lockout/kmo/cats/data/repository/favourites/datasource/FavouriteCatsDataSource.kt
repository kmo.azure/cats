package pro.lockout.kmo.cats.data.repository.favourites.datasource

import io.reactivex.Completable
import io.reactivex.Single
import pro.lockout.kmo.cats.data.model.cache.CatCacheDataModel

interface FavouriteCatsDataSource {

    /**
     * Получение всех избранных котов
     *
     * @return список избранных котов
     */
    fun fetch(): Single<List<CatCacheDataModel>>

    /**
     * Добавляет кота в избранное
     *
     * @param cat кот из избранного
     */
    fun retain(cat: CatCacheDataModel): Completable

}