package pro.lockout.kmo.cats.presentation.favourites

import pro.lockout.kmo.cats.presentation.abs.Presenter

interface FavouriteCatsPresenter: Presenter<FavouriteCatsView> {

    /**
     * Скачивание картинки кота
     *
     * @param url путь до картинки
     */
    fun download(url: String)

}