package pro.lockout.kmo.cats.domain.interactor.favourites

import io.reactivex.Completable
import io.reactivex.Single
import pro.lockout.kmo.cats.domain.model.CatDomainModel

interface FavouriteCatsInteractor {

    /**
     * Получение списка котов из избранного
     */
    fun fetch(): Single<List<CatDomainModel>>

    /**
     * Добавление кота в избранное
     *
     * @param cat выбранный кот
     */
    fun retain(cat: CatDomainModel): Completable

}