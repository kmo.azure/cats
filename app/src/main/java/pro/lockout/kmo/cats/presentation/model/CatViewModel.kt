package pro.lockout.kmo.cats.presentation.model

import pro.lockout.kmo.cats.domain.model.CatDomainModel

data class CatViewModel(
    val id: String,
    val url: String,
    var isFavourite: Boolean?) {

    companion object {

        fun map(cat: CatDomainModel): CatViewModel =
            CatViewModel(
                cat.id,
                cat.url,
                cat.isFavourite)

        fun map(cats: List<CatDomainModel>): List<CatViewModel> =
            cats.map(::map)

        fun map(cat: CatViewModel): CatDomainModel =
            CatDomainModel(
                cat.id,
                cat.url,
                cat.isFavourite)

    }

}