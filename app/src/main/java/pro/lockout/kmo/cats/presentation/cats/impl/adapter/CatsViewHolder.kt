package pro.lockout.kmo.cats.presentation.cats.impl.adapter

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.cats__cat.view.*
import pro.lockout.kmo.cats.R
import pro.lockout.kmo.cats.auxiliary.extensions.gone
import pro.lockout.kmo.cats.auxiliary.extensions.visible
import pro.lockout.kmo.cats.presentation.cats.impl.adapter.CatsAdapter.*
import pro.lockout.kmo.cats.presentation.model.CatViewModel

class CatsViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {

    fun bind(cat: CatViewModel, delegate: Delegate?, downloadDelegate: DownloadDelegate, position: Int) {
        itemView.apply {
            Glide.with(this)
                .load(cat.url)
                .into(catImage)

            when (cat.isFavourite) {
                null -> {
                    favourite.gone()
                }

                true -> {
                    favourite.setImageResource(R.drawable.ic_favourite)
                    favourite.visible()
                }

                false -> {
                    favourite.setImageResource(R.drawable.ic_not_favourite)
                    favourite.visible()
                }
            }

            favourite.setOnClickListener {
                cat.isFavourite = !cat.isFavourite!!

                favourite.setImageResource(
                    if (cat.isFavourite!!) {
                        R.drawable.ic_favourite
                    } else {
                        R.drawable.ic_not_favourite
                    })

                delegate?.onAddFavouritePicked(cat)
            }

            download.setOnClickListener {
                downloadDelegate.onDownloadImagePicket(cat.url)
            }

            delegate?.onUploadCats(position)
        }
    }

}