package pro.lockout.kmo.cats.domain.interactor.download

import io.reactivex.Completable

interface DownloadInteractor {

    /**
     * Скачивание документа в загрузки
     *
     * @param url путь к документу
     */
    fun download(url: String): Completable

}