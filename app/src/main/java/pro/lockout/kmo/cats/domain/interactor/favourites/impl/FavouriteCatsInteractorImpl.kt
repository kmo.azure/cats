package pro.lockout.kmo.cats.domain.interactor.favourites.impl

import io.reactivex.Completable
import io.reactivex.Single
import pro.lockout.kmo.cats.data.model.cache.CatCacheDataModel
import pro.lockout.kmo.cats.data.repository.favourites.FavouriteCatsRepository
import pro.lockout.kmo.cats.domain.interactor.favourites.FavouriteCatsInteractor
import pro.lockout.kmo.cats.domain.model.CatDomainModel
import javax.inject.Inject

class FavouriteCatsInteractorImpl
    @Inject constructor(
        private val favouriteCatsRepository: FavouriteCatsRepository):
            FavouriteCatsInteractor {

    override fun fetch(): Single<List<CatDomainModel>> =
        favouriteCatsRepository.fetch()
            .map(CatDomainModel.Companion::mapCache)

    override fun retain(cat: CatDomainModel): Completable =
        favouriteCatsRepository.retain(CatCacheDataModel.map(cat))

}