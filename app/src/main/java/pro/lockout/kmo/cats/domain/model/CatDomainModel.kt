package pro.lockout.kmo.cats.domain.model

import pro.lockout.kmo.cats.data.model.cache.CatCacheDataModel
import pro.lockout.kmo.cats.data.model.cloud.CatCloudDataModel

data class CatDomainModel(
    val id: String,
    val url: String,
    val isFavourite: Boolean?) {

    companion object {

        fun map(cat: CatCloudDataModel): CatDomainModel =
            CatDomainModel(
                cat.id ?: "",
                cat.url ?: "",
                false)

        fun map(cats: List<CatCloudDataModel>): List<CatDomainModel> =
            cats.map(::map)

        fun mapCache(cat: CatCacheDataModel): CatDomainModel =
            CatDomainModel(
                cat.id,
                cat.url,
                null)

        fun mapCache(cats: List<CatCacheDataModel>): List<CatDomainModel> =
            cats.map(::mapCache)

    }

}