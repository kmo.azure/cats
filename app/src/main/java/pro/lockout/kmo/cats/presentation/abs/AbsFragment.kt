package pro.lockout.kmo.cats.presentation.abs

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.tbruyelle.rxpermissions2.RxPermissions
import dagger.android.support.DaggerFragment
import io.reactivex.disposables.CompositeDisposable
import pro.lockout.kmo.cats.auxiliary.extensions.addTo
import pro.lockout.kmo.cats.auxiliary.extensions.dismissKeyboard
import pro.lockout.kmo.cats.auxiliary.schedulers.Schedulers
import javax.inject.Inject

abstract class AbsFragment: DaggerFragment() {

    @Inject protected lateinit var schedulers: Schedulers

    protected abstract val layout: Int
    protected lateinit var disposables: CompositeDisposable
    private val rxPermissions: RxPermissions by lazy { RxPermissions(activity!!) }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? =
        inflater.inflate(layout, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        disposables = CompositeDisposable()

        view.requestFocus()

        view.dismissKeyboard()
    }

    protected fun checkPermission(permission: String, action: () -> Unit) {
        rxPermissions.request(permission)
            .subscribe(
                { isGranted ->
                    if (isGranted) {
                        action()
                    }
                },
                { /* Пустая реализация */ })
            .addTo(disposables)
    }

    override fun onDestroyView() {
        super.onDestroyView()

        disposables.dispose()
        view?.dismissKeyboard()
    }

}