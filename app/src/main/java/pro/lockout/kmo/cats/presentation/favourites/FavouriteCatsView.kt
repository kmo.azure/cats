package pro.lockout.kmo.cats.presentation.favourites

import pro.lockout.kmo.cats.presentation.abs.View
import pro.lockout.kmo.cats.presentation.model.CatViewModel

interface FavouriteCatsView: View {

    /**
     * Отображение списка избранных котов
     *
     * @param favouriteCats избранные коты
     */
    fun showFavouriteCats(favouriteCats: List<CatViewModel>)

}