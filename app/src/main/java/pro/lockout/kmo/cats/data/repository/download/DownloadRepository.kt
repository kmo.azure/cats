package pro.lockout.kmo.cats.data.repository.download

import io.reactivex.Completable

interface DownloadRepository {

    /**
     * Скачивание документа в загрузки
     *
     * @param url путь к документу
     */
    fun download(url: String): Completable

}