package pro.lockout.kmo.cats.auxiliary.extensions

import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable

fun Disposable.addTo(disposable: CompositeDisposable) =
        apply { disposable.add(this) }