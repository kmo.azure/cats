package pro.lockout.kmo.cats.presentation.cats

import pro.lockout.kmo.cats.presentation.abs.Presenter
import pro.lockout.kmo.cats.presentation.model.CatViewModel

interface CatsPresenter: Presenter<CatsView> {

    /**
     * Получение списка котов
     *
     * @param catPosition позиция
     * текущего кота в списке
     * @param catsCount количество
     * котов в списке
     */
    fun displayCats(catPosition: Int, catsCount: Int)

    /**
     * Переход на экран с избранными котами
     */
    fun toFavourites()

    /**
     * Добавление/удаление кота в/из избранных
     *
     * @param cat избранный кот
     */
    fun addFavouriteCat(cat: CatViewModel)

    /**
     * Скачивание картинки кота
     *
     * @param url путь до картинки
     */
    fun download(url: String)

}