package pro.lockout.kmo.cats.auxiliary.schedulers

import io.reactivex.Scheduler
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers.computation

class DefaultSchedulers: Schedulers {

    override fun ui(): Scheduler =
        AndroidSchedulers.mainThread()

    override fun background(): Scheduler =
        computation()

}