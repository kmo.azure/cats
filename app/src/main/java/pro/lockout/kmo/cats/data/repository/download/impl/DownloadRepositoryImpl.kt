package pro.lockout.kmo.cats.data.repository.download.impl

import io.reactivex.Completable
import pro.lockout.kmo.cats.data.repository.download.DownloadRepository
import pro.lockout.kmo.cats.data.repository.download.datasource.DownloadDataSource
import javax.inject.Inject

class DownloadRepositoryImpl
    @Inject constructor(
        private val downloadDataSource: DownloadDataSource):
            DownloadRepository {

    override fun download(url: String): Completable =
        downloadDataSource.download(url)

}