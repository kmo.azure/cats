package pro.lockout.kmo.cats.data.repository.download.datasource.impl

import android.content.Context
import io.reactivex.Completable
import pro.lockout.kmo.cats.data.repository.download.datasource.DownloadDataSource
import javax.inject.Inject
import android.app.DownloadManager
import android.app.DownloadManager.*
import android.app.DownloadManager.Request.*
import android.net.Uri
import android.os.Environment.*
import pro.lockout.kmo.cats.R
import java.io.File

class DownloadDataSourceImpl
    @Inject constructor(
        private val context: Context):
            DownloadDataSource {

    override fun download(url: String): Completable =
        Completable.fromCallable {
            val direct = File(DIRECTORY_DOWNLOADS)

            if (!direct.exists()) {
                direct.mkdirs()
            }

            val uri = Uri.parse(url)

            val request = Request(uri)
                .setAllowedNetworkTypes(NETWORK_WIFI or NETWORK_MOBILE)
                .setAllowedOverRoaming(false)
                .setVisibleInDownloadsUi(true)
                .setTitle(uri.lastPathSegment)
                .setDescription(uri.lastPathSegment)
                .setDestinationInExternalPublicDir(DIRECTORY_DOWNLOADS,
                    context.getString(R.string.download_file_extension_jpg, uri.lastPathSegment))

            (context.getSystemService(Context.DOWNLOAD_SERVICE) as DownloadManager).enqueue(request)
        }

}