package pro.lockout.kmo.cats.presentation.cats

import pro.lockout.kmo.cats.presentation.abs.View
import pro.lockout.kmo.cats.presentation.model.CatViewModel

interface CatsView: View {

    /**
     * Отображение списка котов в ленте
     *
     * @param cats список котов
     */
    fun showCats(cats: List<CatViewModel>)

}