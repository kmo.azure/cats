package pro.lockout.kmo.cats.auxiliary.di.modules

import android.content.Context
import dagger.Module
import dagger.Provides
import pro.lockout.kmo.cats.data.network.api.NetworkApi
import pro.lockout.kmo.cats.data.repository.cats.CatsRepository
import pro.lockout.kmo.cats.data.repository.cats.datasource.CatsDataSource
import pro.lockout.kmo.cats.data.repository.cats.datasource.impl.CatsDataSourceCloud
import pro.lockout.kmo.cats.data.repository.cats.impl.CatsRepositoryImpl
import pro.lockout.kmo.cats.data.repository.download.DownloadRepository
import pro.lockout.kmo.cats.data.repository.download.datasource.DownloadDataSource
import pro.lockout.kmo.cats.data.repository.download.datasource.impl.DownloadDataSourceImpl
import pro.lockout.kmo.cats.data.repository.download.impl.DownloadRepositoryImpl
import pro.lockout.kmo.cats.data.repository.favourites.FavouriteCatsRepository
import pro.lockout.kmo.cats.data.repository.favourites.datasource.FavouriteCatsDataSource
import pro.lockout.kmo.cats.data.repository.favourites.datasource.impl.FavouriteCatsDataSourceCache
import pro.lockout.kmo.cats.data.repository.favourites.impl.FavouriteCatsRepositoryImpl
import retrofit2.Retrofit
import javax.inject.Singleton

@Module(includes = [ NetworkModule::class ])
class DataModule {

    @Singleton
    @Provides
    fun provideCatsDataSource(
        retrofit: Retrofit): CatsDataSource =
            CatsDataSourceCloud(retrofit.create(NetworkApi::class.java))

    @Singleton
    @Provides
    fun provideCatsRepository(
        catsDataSource: CatsDataSource): CatsRepository =
            CatsRepositoryImpl(catsDataSource)

    @Singleton
    @Provides
    fun provideFavouriteCatsDataSource(
        context: Context): FavouriteCatsDataSource =
            FavouriteCatsDataSourceCache(context)

    @Singleton
    @Provides
    fun provideFavouriteCatsRepository(
        favouriteCatsDataSource: FavouriteCatsDataSource): FavouriteCatsRepository =
            FavouriteCatsRepositoryImpl(favouriteCatsDataSource)

    @Singleton
    @Provides
    fun provideDownloadDataSource(
        context: Context): DownloadDataSource =
            DownloadDataSourceImpl(context)

    @Singleton
    @Provides
    fun provideDownloadRepository(
        downloadDataSource: DownloadDataSource): DownloadRepository =
            DownloadRepositoryImpl(downloadDataSource)

}