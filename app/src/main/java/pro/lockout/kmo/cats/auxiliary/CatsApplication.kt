package pro.lockout.kmo.cats.auxiliary

import dagger.android.AndroidInjector
import dagger.android.DaggerApplication
import io.reactivex.internal.functions.Functions
import io.reactivex.plugins.RxJavaPlugins
import io.realm.Realm
import io.realm.RealmConfiguration
import pro.lockout.kmo.cats.R
import pro.lockout.kmo.cats.auxiliary.di.DaggerAppComponent
import pro.lockout.kmo.cats.auxiliary.schedulers.Schedulers
import pro.lockout.kmo.cats.data.realm.RealmModule

class CatsApplication: DaggerApplication() {

    override fun applicationInjector(): AndroidInjector<out DaggerApplication> =
        DaggerAppComponent
            .builder()
            .withContext(this)
            .withSchedulers(Schedulers.schedulers())
            .withResources(resources)
            .build()

    override fun onCreate() {
        super.onCreate()

        RxJavaPlugins.setErrorHandler(Functions.emptyConsumer())
        initRealm()
    }

    private fun initRealm() {
        Realm.init(this)

        val realmConfiguration = RealmConfiguration.Builder()
            .name(getString(R.string.realm_database))
            .modules(RealmModule())
            .deleteRealmIfMigrationNeeded()
            .build()

        try {
            Realm.setDefaultConfiguration(realmConfiguration)
            Realm.getDefaultInstance().close()
        } catch (e: Exception) {
            Realm.deleteRealm(realmConfiguration)
        }

    }

}