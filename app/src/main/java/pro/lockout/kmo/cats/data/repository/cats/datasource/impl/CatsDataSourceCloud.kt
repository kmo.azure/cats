package pro.lockout.kmo.cats.data.repository.cats.datasource.impl

import io.reactivex.Single
import pro.lockout.kmo.cats.data.model.cloud.CatCloudDataModel
import pro.lockout.kmo.cats.data.network.api.NetworkApi
import pro.lockout.kmo.cats.data.repository.cats.datasource.CatsDataSource
import javax.inject.Inject

class CatsDataSourceCloud
    @Inject constructor(
        private val api: NetworkApi):
            CatsDataSource {

    override fun fetch(limit: Int, page: Int, order: String): Single<List<CatCloudDataModel>> =
        api.getCats(limit, page, order)

}