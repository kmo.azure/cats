package pro.lockout.kmo.cats.domain.interactor.cats.impl

import io.reactivex.Single
import pro.lockout.kmo.cats.data.repository.cats.CatsRepository
import pro.lockout.kmo.cats.domain.interactor.cats.CatsInteractor
import pro.lockout.kmo.cats.domain.model.CatDomainModel
import javax.inject.Inject

class CatsInteractorImpl
    @Inject constructor(
        private val catsRepository: CatsRepository):
            CatsInteractor {

    private companion object {

        const val CATS_COUNT_FOR_UPDATE = 20
        const val CATS_PER_PAGE = 100

    }

    override fun fetch(currentCatPosition: Int, catsCount: Int): Single<List<CatDomainModel>> =
        if (catsCount == 0 || currentCatPosition == catsCount - CATS_COUNT_FOR_UPDATE) {
            catsRepository.fetch(CATS_PER_PAGE, catsCount / CATS_PER_PAGE)
                .map(CatDomainModel.Companion::map)
        } else {
            Single.never()
        }

}