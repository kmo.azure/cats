package pro.lockout.kmo.cats.data.repository.cats.datasource

import io.reactivex.Single
import pro.lockout.kmo.cats.data.model.cloud.CatCloudDataModel

interface CatsDataSource {

    /**
     * Получение списка котов
     *
     * @param limit количество котов
     * получаемых в одном запросе
     * @param page страница с котами
     * @param order тип сортировки
     *
     * @return список котов по параметрам
     */
    fun fetch(limit: Int = 100, page: Int = 0, order: String = "desc"): Single<List<CatCloudDataModel>>

}