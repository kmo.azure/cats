package pro.lockout.kmo.cats.presentation.cats.impl

import androidx.fragment.app.FragmentManager
import pro.lockout.kmo.cats.presentation.cats.CatsRouter
import pro.lockout.kmo.cats.presentation.favourites.impl.FavouriteCatsFragment

class CatsRouterImpl(
    override val fragmentManager: FragmentManager?):
        CatsRouter {

    override fun toFavouriteCats() {
        push(FavouriteCatsFragment.newInstance())
    }

}