package pro.lockout.kmo.cats.data.realm

import io.realm.annotations.RealmModule
import pro.lockout.kmo.cats.data.model.cache.CatCacheDataModel

@RealmModule(library = true, classes = [ CatCacheDataModel::class ])
class RealmModule