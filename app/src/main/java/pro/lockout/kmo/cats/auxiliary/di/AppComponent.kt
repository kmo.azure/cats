package pro.lockout.kmo.cats.auxiliary.di

import android.content.Context
import android.content.res.Resources
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjectionModule
import dagger.android.AndroidInjector
import pro.lockout.kmo.cats.auxiliary.CatsApplication
import pro.lockout.kmo.cats.auxiliary.di.modules.Module
import pro.lockout.kmo.cats.auxiliary.schedulers.Schedulers
import javax.inject.Singleton

@Singleton
@Component(modules = [
    AndroidInjectionModule::class,
    Module::class ])
interface AppComponent: AndroidInjector<CatsApplication> {

    @Component.Builder
    interface Builder {

        @BindsInstance
        fun withContext(context: Context): Builder

        @BindsInstance
        fun withSchedulers(schedulers: Schedulers): Builder

        @BindsInstance
        fun withResources(resources: Resources): Builder

        fun build(): AppComponent

    }

}