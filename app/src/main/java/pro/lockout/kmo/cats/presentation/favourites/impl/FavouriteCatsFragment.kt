package pro.lockout.kmo.cats.presentation.favourites.impl

import android.Manifest.permission.WRITE_EXTERNAL_STORAGE
import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import kotlinx.android.synthetic.main.error.*
import kotlinx.android.synthetic.main.favourite_cats.*
import kotlinx.android.synthetic.main.loading.*
import pro.lockout.kmo.cats.R
import pro.lockout.kmo.cats.auxiliary.extensions.gone
import pro.lockout.kmo.cats.auxiliary.extensions.visible
import pro.lockout.kmo.cats.domain.interactor.download.DownloadInteractor
import pro.lockout.kmo.cats.domain.interactor.favourites.FavouriteCatsInteractor
import pro.lockout.kmo.cats.presentation.abs.AbsFragment
import pro.lockout.kmo.cats.presentation.cats.impl.adapter.CatsAdapter
import pro.lockout.kmo.cats.presentation.favourites.FavouriteCatsPresenter
import pro.lockout.kmo.cats.presentation.favourites.FavouriteCatsView
import pro.lockout.kmo.cats.presentation.model.CatViewModel
import javax.inject.Inject

class FavouriteCatsFragment: AbsFragment(), FavouriteCatsView,
    CatsAdapter.DownloadDelegate {

    companion object {

        fun newInstance(): Fragment = FavouriteCatsFragment().apply {
            arguments = Bundle.EMPTY
        }

    }

    override val layout: Int = R.layout.favourite_cats

    @Inject lateinit var favouriteCatsInteractor: FavouriteCatsInteractor
    @Inject lateinit var downloadInteractor: DownloadInteractor

    private val presenter: FavouriteCatsPresenter by lazy {
        FavouriteCatsPresenterImpl(favouriteCatsInteractor, downloadInteractor, schedulers)
    }

    private val adapter = CatsAdapter(downloadDelegate = this)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        favouriteCatsList.adapter = adapter

        presenter.attachView(this)
    }

    override fun showFavouriteCats(favouriteCats: List<CatViewModel>) {
        adapter.updateData(favouriteCats)
        showContent()
    }

    override fun onDownloadImagePicket(url: String) {
        checkPermission(WRITE_EXTERNAL_STORAGE) {
            presenter.download(url)
        }
    }

    override fun showLoading() {
        error.gone()
        loading.visible()
    }

    override fun showContent() {
        error.gone()
        loading.gone()
    }

    override fun showError(errorValue: Any?) {
        (errorValue as? String)?.let {
            error.text = errorValue
        }

        error.visible()
        loading.gone()
    }

    override fun onDestroyView() {
        super.onDestroyView()

        presenter.destroyView()
    }

}