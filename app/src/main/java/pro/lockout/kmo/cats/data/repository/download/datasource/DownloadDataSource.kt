package pro.lockout.kmo.cats.data.repository.download.datasource

import io.reactivex.Completable

interface DownloadDataSource {

    /**
     * Скачивание документа в загрузки
     *
     * @param url путь к документу
     */
    fun download(url: String): Completable

}