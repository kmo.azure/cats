package pro.lockout.kmo.cats.data.repository.cats.impl

import io.reactivex.Single
import pro.lockout.kmo.cats.data.model.cloud.CatCloudDataModel
import pro.lockout.kmo.cats.data.repository.cats.CatsRepository
import pro.lockout.kmo.cats.data.repository.cats.datasource.CatsDataSource
import javax.inject.Inject

class CatsRepositoryImpl
    @Inject constructor(
        private val catsDataSource: CatsDataSource):
            CatsRepository {

    override fun fetch(limit: Int, page: Int): Single<List<CatCloudDataModel>> =
        catsDataSource.fetch(limit = limit, page = page)

}