package pro.lockout.kmo.cats.data.model.cloud

data class CatCloudDataModel(
    val id: String? = "",
    val url: String? = "")