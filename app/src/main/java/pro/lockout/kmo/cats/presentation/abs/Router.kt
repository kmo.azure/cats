package pro.lockout.kmo.cats.presentation.abs

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager

interface Router {

    val fragmentManager: FragmentManager?

    fun push(fragment: Fragment) {
        fragmentManager
            ?.beginTransaction()
            ?.add(
                android.R.id.content,
                fragment,
                fragment::class.java.name)
            ?.addToBackStack(fragment::class.java.name)
            ?.commit()
    }

}