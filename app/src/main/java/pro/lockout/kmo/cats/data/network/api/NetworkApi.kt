package pro.lockout.kmo.cats.data.network.api

import io.reactivex.Single
import pro.lockout.kmo.cats.data.model.cloud.CatCloudDataModel
import retrofit2.http.GET
import retrofit2.http.Query

interface NetworkApi {

    @GET("/v1/images/search")
    fun getCats(
        @Query("limit") limit: Int,
        @Query("page") page: Int,
        @Query("order") order: String):
            Single<List<CatCloudDataModel>>

}