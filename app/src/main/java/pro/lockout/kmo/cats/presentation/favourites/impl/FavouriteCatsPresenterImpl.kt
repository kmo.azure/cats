package pro.lockout.kmo.cats.presentation.favourites.impl

import io.reactivex.disposables.CompositeDisposable
import pro.lockout.kmo.cats.auxiliary.extensions.addTo
import pro.lockout.kmo.cats.auxiliary.schedulers.Schedulers
import pro.lockout.kmo.cats.domain.interactor.download.DownloadInteractor
import pro.lockout.kmo.cats.domain.interactor.favourites.FavouriteCatsInteractor
import pro.lockout.kmo.cats.presentation.favourites.FavouriteCatsPresenter
import pro.lockout.kmo.cats.presentation.favourites.FavouriteCatsView
import pro.lockout.kmo.cats.presentation.model.CatViewModel

class FavouriteCatsPresenterImpl(
    private val favouriteCatsInteractor: FavouriteCatsInteractor,
    private val downloadInteractor: DownloadInteractor,
    private val schedulers: Schedulers):
        FavouriteCatsPresenter {

    override var attachedView: FavouriteCatsView? = null
    override var disposable = CompositeDisposable()

    override fun attachView(view: FavouriteCatsView?) {
        super.attachView(view)

        attachedView?.showLoading()
        displayCats()
    }

    private fun displayCats() {
        favouriteCatsInteractor.fetch()
            .map(CatViewModel.Companion::map)
            .observeOn(schedulers.ui())
            .subscribeOn(schedulers.background())
            .subscribe(
                { attachedView?.showFavouriteCats(it) },
                { attachedView?.showError(it.message) })
            .addTo(disposable)
    }

    override fun download(url: String) {
        downloadInteractor.download(url)
            .observeOn(schedulers.ui())
            .subscribeOn(schedulers.background())
            .subscribe()
            .addTo(disposable)
    }

}