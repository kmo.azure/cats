package pro.lockout.kmo.cats.presentation.cats

import pro.lockout.kmo.cats.presentation.abs.Router

interface CatsRouter: Router {

    /**
     * Переход на экран с избранными
     * котами
     */
    fun toFavouriteCats()

}