package pro.lockout.kmo.cats.presentation.cats.impl.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import pro.lockout.kmo.cats.R
import pro.lockout.kmo.cats.presentation.model.CatViewModel

class CatsAdapter(
    private var cats: MutableList<CatViewModel> = mutableListOf(),
    private val delegate: Delegate? = null,
    private val downloadDelegate: DownloadDelegate):
        RecyclerView.Adapter<CatsViewHolder>() {

    interface Delegate {

        /**
         * Событие срабатывает, когда
         * необходимо
         *
         * @param catPosition номер кота
         * в списке
         */
        fun onUploadCats(catPosition: Int)

        /**
         * Событие срабатывает, когда пользователь
         * добавляет картинку в избранное
         *
         * @param cat выбранный кот
         */
        fun onAddFavouritePicked(cat: CatViewModel)

    }

    interface DownloadDelegate {

        /**
         * Событие срабатывает, когда пользователь
         * собирается скачать картинку
         *
         * @param url путь к картинке
         */
        fun onDownloadImagePicket(url: String)

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CatsViewHolder =
        CatsViewHolder(LayoutInflater.from(parent.context)
            .inflate(R.layout.cats__cat, parent, false))

    override fun getItemCount(): Int =
        cats.size

    override fun onBindViewHolder(holder: CatsViewHolder, position: Int) =
        holder.bind(cats[position], delegate, downloadDelegate, position)

    fun updateData(newCats: List<CatViewModel>) {
        cats.addAll(newCats)
        notifyDataSetChanged()
    }

}