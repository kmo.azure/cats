package pro.lockout.kmo.cats.data.repository.favourites.datasource.impl

import android.content.Context
import io.reactivex.Completable
import io.reactivex.Single
import io.realm.Realm
import pro.lockout.kmo.cats.R
import pro.lockout.kmo.cats.data.exception.FavouriteCatsNotFoundException
import pro.lockout.kmo.cats.data.model.cache.CatCacheDataModel
import pro.lockout.kmo.cats.data.repository.favourites.datasource.FavouriteCatsDataSource

class FavouriteCatsDataSourceCache(
    private val context: Context):
        FavouriteCatsDataSource {

    override fun fetch(): Single<List<CatCacheDataModel>> =
        Single.fromCallable {
            Realm.getDefaultInstance()
                .use(::findAllCats)
                .apply {
                    if (isEmpty()) {
                        throw FavouriteCatsNotFoundException(context.getString(R.string.favourite_cats_not_found))
                    }
                }
        }

    override fun retain(cat: CatCacheDataModel): Completable =
        Completable.fromCallable {
            Realm.getDefaultInstance().use { storage ->
                storage.executeTransaction {
                    val cats = removeOrAddCat(findAllCats(storage), cat)

                    storage.delete(CatCacheDataModel::class.java)
                    storage.copyToRealm(cats)
                }
            }
        }

    private fun findAllCats(storage: Realm): MutableList<CatCacheDataModel> =
        storage.copyFromRealm(
            storage
                .where(CatCacheDataModel::class.java)
                .findAll())

    private fun removeOrAddCat(cats: MutableList<CatCacheDataModel>, cat: CatCacheDataModel): MutableList<CatCacheDataModel> {
        val cacheCat = cats.find { it.id == cat.id }

        if (cacheCat != null) {
            cats.remove(cacheCat)
        } else {
            cats.add(cat)
        }

        return cats
    }

}