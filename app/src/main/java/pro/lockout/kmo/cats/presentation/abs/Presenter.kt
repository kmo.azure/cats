package pro.lockout.kmo.cats.presentation.abs

import io.reactivex.disposables.CompositeDisposable

interface Presenter<V: View> {

    var attachedView: V?
    var disposable: CompositeDisposable

    fun attachView(view: V?) {
        attachedView = view
        disposable = CompositeDisposable()
    }

    fun destroyView() {
        attachedView = null
        disposable.dispose()
    }

}