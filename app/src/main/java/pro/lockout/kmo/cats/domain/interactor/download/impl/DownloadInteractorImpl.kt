package pro.lockout.kmo.cats.domain.interactor.download.impl

import io.reactivex.Completable
import pro.lockout.kmo.cats.data.repository.download.DownloadRepository
import pro.lockout.kmo.cats.domain.interactor.download.DownloadInteractor
import javax.inject.Inject

class DownloadInteractorImpl
    @Inject constructor(
        private val downloadRepository: DownloadRepository):
            DownloadInteractor {

    override fun download(url: String): Completable =
        downloadRepository.download(url)

}