package pro.lockout.kmo.cats.auxiliary.di.modules

import dagger.Module
import dagger.android.ContributesAndroidInjector
import pro.lockout.kmo.cats.presentation.MainActivity
import pro.lockout.kmo.cats.presentation.cats.impl.CatsFragment
import pro.lockout.kmo.cats.presentation.favourites.impl.FavouriteCatsFragment

@Module(includes = [ DomainModule::class ])
interface Module {

    @ContributesAndroidInjector
    fun contributeMainActivityInjector(): MainActivity

    @ContributesAndroidInjector
    fun contributeCatsFragmentInjector(): CatsFragment

    @ContributesAndroidInjector
    fun contributeFavouriteCatsFragmentInjector(): FavouriteCatsFragment

}